// JSON BASE A MOSTRAR EN FORMULARIO
function init() {
  /**
   * Convierte el JSON a string para poder mostrarlo
   * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
   */
    var JsonString = JSON.stringify(baseJSON, null, 2);
    document.getElementById("description").value = JsonString;

  // SE LISTAN TODOS LOS PRODUCTOS
  // listarProductos();
}
var baseJSON = {
    precio: 0.0,
    unidades: 1,
    modelo: "XX-000",
    marca: "NA",
    detalles: "NA",
    imagen: "img/porDefecto.png",
};
$(function () {

    let editar = false;
    // Mostramos
    fetchProducts();

    console.log("JQuery funcionando");
    $('#product-result').hide();
    $('#search').keyup(function (e) {
        if($('#search').val()){
            let search = $('#search').val()
            $.ajax({
                url: 'backend/product-search.php',
                type: 'GET',
                data: { search },
                success: function (response){
                    let productos = JSON.parse(response);
                    let template = '';
                    productos.forEach(producto => {
                        template += `<li>
                            ${producto.nombre}
                        </li>`
                    });
                    $('#container').html(template);
                    $('#product-result').show();
                }
            });
        }
    });

    $('#product-form').submit(function(e){
        var productoJsonString = document.getElementById("description").value;

        console.log(productoJsonString);
        // SE CONVIERTE EL JSON DE STRING A OBJETO
        var finalJSON = JSON.parse(productoJsonString);
        // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
        finalJSON["nombre"] = document.getElementById("name").value;
        // SE OBTIENE EL STRING DEL JSON FINAL
        productoJsonString = JSON.stringify(finalJSON, null, 2);
      
        var datosProductos = JSON.parse(productoJsonString);

        let nombre = datosProductos["nombre"];
        let imagen = datosProductos["imagen"];
        let precio = parseFloat(datosProductos["precio"]);
        let unidades = parseInt(datosProductos["unidades"]);
        let modelo = datosProductos["modelo"];
        let marca = datosProductos["marca"];
        let detalles = datosProductos["detalles"];

        var errrores = [];
    
        if (nombre === null || nombre.length >= 100) {
            errrores.push("-Ingresa un nombre valido\n");
        }
        if (marca === "0") {
            errrores.push("-Selecciona una marca\n");
        }
        if (modelo === null || modelo.length >= 25) {
            errrores.push("-Ingresa una descripcion valida\n");
        }
        if (precio <= 99.99) {
            console.log(precio.value);
            errrores.push("-Ingresa un precio mayor a $99.99\n");
        }
        if (detalles.length <= 0 || detalles.length >= 250) {
            errrores.push("-Ingresa una descripcion valida\n");
        }
        if (unidades <= 0) {
            errrores.push("-Ingresa una cantidad valida\n");
        }
        if (imagen === null || imagen === "" || imagen.length <= 0) {
            //errrores.push("-Ingresa una direccion valida\n");
            imagen.value = "img/porDefecto.png";
            console.log(imagen.value);
        }
    
        if (errrores.length > 0) {
            alert(errrores.join(" "));
        }else{
            const postData = {
                nombre: datosProductos["nombre"],
                precio: datosProductos["precio"],
                unidades: datosProductos["unidades"],
                modelo: datosProductos["modelo"],
                marca: datosProductos["marca"],
                detalles: datosProductos["detalles"],
                imagen: datosProductos["imagen"],
                id: $('#productID').val()
            };
            console.log(postData);

            let url = editar == false ? 'backend/product-add.php' : 'backend/product-edit.php';

            $.post(url, postData, function(response) {
                console.log(response);
                let productos = JSON.parse(response);
                let template = '';
                productos.forEach(elemento => {
                    template += `<li>${elemento.status}</li>`
                });
                $('#container').html(template);
                fetchProducts();
                $('#product-form').trigger('reset');
            });
            e.preventDefault();            
        }
    });

    function fetchProducts(){
        $.ajax({
            url: 'backend/product-list.php',
            type: 'GET',
            success: function(response) {
                let template = '';
                let productos = JSON.parse(response);
                productos.forEach(producto => {
                    template += `
                        <tr productId="${producto.id}">
                            <td>${producto.id}</td>
                            <td>
                                <a href="#" class="product-item" >${producto.nombre}</a>
                            </td>
                            <td>${producto.detalles}</td>
                            <td>
                                <button class="product-delete btn btn-danger">
                                    Eliminar
                                </button>
                            </td>
                        </tr>
                    `
                });
                $('#products').html(template);
            }
        });
    }

    // Eliminar
    $(document).on('click', '.product-delete', function(e) {
        if(confirm('Are you sure you want to delete it?')){
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('productId');
            console.log(id);
            $.post('backend/product-delete.php', {id}, function(response) {
                console.log(response);
                fetchProducts();
            });
        }
    });

    // 
    $(document).on('click', '.product-item', function(e) {
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');
        $.post('backend/product-single.php', {id}, function(response) {
            console.log(response);
            const producto = JSON.parse(response);
            $('#productID').val(producto.id)
            $('#name').val(producto.nombre);
            $('#description').val(`{
                "precio": ${producto.precio},
                "unidades": ${producto.unidades},
                "modelo": "${producto.modelo}",
                "marca": "${producto.marca}",
                "detalles": "${producto.detalles}",
                "imagen": "${producto.imagen}"
            }`);
            editar = true;
        });
        e.preventDefault();
    });

});
