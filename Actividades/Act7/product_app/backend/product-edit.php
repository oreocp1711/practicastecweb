<?php
    use API\Productos;
    include_once __DIR__.'/API/Productos.php';

    $_edit = json_decode(json_encode($_POST));

    $producto = new Productos();
    $producto->edit($_edit);
    echo $producto->getResponse();

?>