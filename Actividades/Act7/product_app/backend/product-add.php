<?php
    use API\Productos;
    include_once __DIR__.'/API/productos.php';

    $productAdd = json_decode(json_encode($_POST));

    $producto = new Productos();
    $producto->add($productAdd);
    echo $producto->getResponse();
?>