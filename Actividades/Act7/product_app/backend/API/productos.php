<?php
    namespace API;
    include_once __DIR__.'/database.php';

    class Productos extends Database{

        protected $response = array();
        
        public function __construct($_response = null){    
            parent::__construct("marketzone");
            $this->response = $_response;
        }

        public function add($_add){
            $this->response = array(
                'status'  => 'error',
                'message' => 'Ya existe un producto con ese nombre'
            );
            if(isset($_add)){
                // SE TRANSFORMA EL POST A UN STRING EN JSON, Y LUEGO A OBJETO
                $jsonOBJ = json_decode( json_encode($_add));
                // SE ASUME QUE LOS DATOS YA FUERON VALIDADOS ANTES DE ENVIARSE
                $sql = "SELECT * FROM productos WHERE nombre = '{$jsonOBJ->nombre}' AND eliminado = 0";
                $result = $this->conection->query($sql);
                
                if ($result->num_rows == 0) {
                    $this->conection->set_charset("utf8");
                    $sql = "INSERT INTO productos VALUES (null, '{$jsonOBJ->nombre}', '{$jsonOBJ->marca}', '{$jsonOBJ->modelo}', {$jsonOBJ->precio}, '{$jsonOBJ->detalles}', {$jsonOBJ->unidades}, '{$jsonOBJ->imagen}', 0)";
                    if($this->conection->query($sql)){
                        $this->response['status'] =  "success";
                        $this->response['message'] =  "Producto agregado";
                    } else {
                        $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conection);
                    }
                }
                $result->free();
                // Cierra la conexion
                $this->conection->close();
            }
        }

        public function delete($_delete){
            $this->response = array(
                'status'  => 'error',
                'message' => 'La consulta falló'
            );
            // SE VERIFICA HABER RECIBIDO EL ID
            if( isset($_delete) ) {
                $id = $_delete;
                // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
                $sql = "UPDATE productos SET eliminado=1 WHERE id = {$id}";
                if ( $this->conection->query($sql) ) {
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto eliminado";
                } else {
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conection);
                }
                $this->conection->close();
            } 
        }

        public function edit($_edit){
            $this->response = array(
                'status'  => 'error',
                'message' => 'La consulta falló'
            );
            // SE VERIFICA HABER RECIBIDO EL ID
            if( isset($_edit) ) {
                $jsonOBJ = $_edit;

                $id = intval($jsonOBJ->id);
                $nombre = $jsonOBJ->nombre;
                $marca = $jsonOBJ->marca;
                $modelo = $jsonOBJ->modelo;
                $precio = floatval($jsonOBJ->precio);
                $detalles = $jsonOBJ->detalles;
                $unidades = intval($jsonOBJ->unidades);
                $imagen = $_POST['imagen'];
                $eliminado = 0;
            
                $query = "UPDATE productos SET nombre='$nombre', marca='$marca', modelo='$modelo', precio='$precio', 
                detalles='$detalles', unidades=$unidades, imagen='$imagen', eliminado=$eliminado WHERE id=$id";
            

                // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
                $sql =  "UPDATE productos SET nombre='{$nombre}', marca='{$marca}',";
                $sql .= "modelo='{$modelo}', precio={$precio}, detalles='{$detalles}',"; 
                $sql .= "unidades={$unidades}, imagen='{$imagen}' WHERE id={$id}";

                $this->conection->set_charset("utf8");
                
                if ($this->conection->query($sql) ) {
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto actualizado";
                } else {
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conection);
                }
                $this->conection->close();
            } 
        }

        public function list(){
            // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
            if ( $result = $this->conection->query("SELECT * FROM productos WHERE eliminado = 0") ) {
                // SE OBTIENEN LOS RESULTADOS
                $rows = $result->fetch_all(MYSQLI_ASSOC);

                if(!is_null($rows)) {
                    // SE CODIFICAN A UTF-8 LOS DATOS Y SE MAPEAN AL ARREGLO DE RESPUESTA
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conection));
            }
            $this->conection->close();
        }

        public function search($_search){
            // SE CREA EL ARREGLO QUE SE VA A DEVOLVER EN FORMA DE JSON
            $this->response = array();
            // SE VERIFICA HABER RECIBIDO EL ID
            //$elemento->$_GET['search'];
            if( isset($_search)){
                $search = $_search;
                // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
                $sql = "SELECT * FROM productos WHERE (id = '{$search}' OR nombre LIKE '%{$search}%' OR marca LIKE '%{$search}%' OR detalles LIKE '%{$search}%') AND eliminado = 0";
                if ( $result = $this->conection->query($sql) ) {
                    // SE OBTIENEN LOS RESULTADOS
                    $rows = $result->fetch_all(MYSQLI_ASSOC);
                    if(!is_null($rows)) {
                        // SE CODIFICAN A UTF-8 LOS DATOS Y SE MAPEAN AL ARREGLO DE RESPUESTA
                        foreach($rows as $num => $row) {
                            foreach($row as $key => $value) {
                                $this->response[$num][$key] = utf8_encode($value);
                            }
                        }
                    }
                    $result->free();
                } else {
                    die('Query Error: '.mysqli_error($this->conection));
                }
                $this->conection->close();
            } 
        }
        
        public function single($_id){
            // SE CREA EL ARREGLO QUE SE VA A DEVOLVER EN FORMA DE JSON
            $this->response = array();
            if( isset($_id) ) {
                $id = $_id;
                // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
                if ( $result = $this->conection->query("SELECT * FROM productos WHERE id = {$id}") ) {
                    // SE OBTIENEN LOS RESULTADOS
                    $row = $result->fetch_assoc();

                    if(!is_null($row)) {
                        // SE CODIFICAN A UTF-8 LOS DATOS Y SE MAPEAN AL ARREGLO DE RESPUESTA
                        foreach($row as $key => $value) {
                            $this->response[$key] = utf8_encode($value);
                        }
                    }
                    $result->free();
                } else {
                    die('Query Error: '.mysqli_error($this->conection));
                }
                $this->conection->close();
            }
        }

        public function getResponse(){
            return json_encode($this->response, JSON_PRETTY_PRINT);
        }
    }
