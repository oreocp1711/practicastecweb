$(document).ready(function(){

    let edit = false;

    $('#product-result').hide();
    $('#nombreBusqueda').focus();
    listarProductos();
    validar();

    function listarProductos() {
        $.ajax({
            url: './backend/product-list.php',
            //url: 'backend/API/productos.php',
            type: 'GET',
            success: function(response) {
                // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                const productos = JSON.parse(response);
            
                // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
                if(Object.keys(productos).length > 0) {
                    // SE CREA UNA PLANTILLA PARA CREAR LAS FILAS A INSERTAR EN EL DOCUMENTO HTML
                    let template = '';

                    productos.forEach(producto => {
                        // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                        let descripcion = '';
                        descripcion += '<li>precio: '+producto.precio+'</li>';
                        descripcion += '<li>unidades: '+producto.unidades+'</li>';
                        descripcion += '<li>modelo: '+producto.modelo+'</li>';
                        descripcion += '<li>marca: '+producto.marca+'</li>';
                        descripcion += '<li>detalles: '+producto.detalles+'</li>';
                    
                        template += `
                            <tr productId="${producto.id}">
                                <td>${producto.id}</td>
                                <td>
                                    <a href="#" class="product-item">${producto.nombre}</a>
                                </td>
                                <td><ul>${descripcion}</ul></td>
                                <td>
                                    <button class="product-delete btn btn-danger" >
                                        Eliminar
                                    </button>
                                </td>
                            </tr>
                        `;
                    });
                    // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                    $('#products').html(template);
                }
            }
        });
    }

    $('#search').keyup(function() {
        if($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
                url: './backend/product-search.php?search='+$('#search').val(),
                data: {search},
                type: 'GET',
                success: function (response) {
                    if(!response.error) {
                        // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                        const productos = JSON.parse(response);
                        
                        // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
                        if(Object.keys(productos).length > 0) {
                            // SE CREA UNA PLANTILLA PARA CREAR LAS FILAS A INSERTAR EN EL DOCUMENTO HTML
                            let template = '';
                            let template_bar = '';

                            productos.forEach(producto => {
                                // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                                let descripcion = '';
                                descripcion += '<li>precio: '+producto.precio+'</li>';
                                descripcion += '<li>unidades: '+producto.unidades+'</li>';
                                descripcion += '<li>modelo: '+producto.modelo+'</li>';
                                descripcion += '<li>marca: '+producto.marca+'</li>';
                                descripcion += '<li>detalles: '+producto.detalles+'</li>';
                            
                                template += `
                                    <tr productId="${producto.id}">
                                        <td>${producto.id}</td>
                                        <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                        <td><ul>${descripcion}</ul></td>
                                        <td>
                                            <button class="product-delete btn btn-danger">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                `;

                                template_bar += `
                                    <li>${producto.nombre}</il>
                                `;
                            });
                            // SE HACE VISIBLE LA BARRA DE ESTADO
                            $('#product-result').show();
                            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                            $('#container').html(template_bar);
                            // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                            $('#products').html(template);    
                        }
                    }
                }
            });
        }
        else {
            $('#product-result').hide();
        }
    });



    function validar(){
        console.log('Validacion');

            $('#nombreBusqueda').keyup(function(){
                var nombreBusqueda = $('#nombreBusqueda').val();
                    if (nombreBusqueda === null || nombreBusqueda.length >= 100 || nombreBusqueda.length === 0){
                        $('#nombreExiste').text('Ingresa un nombre valido 🔒\n');
                        return;
                    }else{
                        console.log(nombreBusqueda);
                        $.ajax({
                            url: './backend/product-search.php?search='+$('#nombreBusqueda').val(),
                            data: {nombreBusqueda},
                            type: 'GET',
                            success: function (response) {
                                console.log(response);
                                if(!response.error) {
                                    // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                                    const productos = JSON.parse(response);
                                    
                                    // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
                                    if(Object.keys(productos).length > 0) {
                                        $('#nombreExiste').text("-El nombre introducido ya existe 🔒\n");

                                        // SE CREA UNA PLANTILLA PARA CREAR LAS FILAS A INSERTAR EN EL DOCUMENTO HTML
                                        console.log(productos.length);
                                        let template = '';
                                        let template_bar = '';

                                        productos.forEach(producto => {
                                            // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                                            let descripcion = '';
                                            descripcion += '<li>precio: '+producto.precio+'</li>';
                                            descripcion += '<li>unidades: '+producto.unidades+'</li>';
                                            descripcion += '<li>modelo: '+producto.modelo+'</li>';
                                            descripcion += '<li>marca: '+producto.marca+'</li>';
                                            descripcion += '<li>detalles: '+producto.detalles+'</li>';
                                        
                                            template += `
                                                <tr productId="${producto.id}">
                                                    <td>${producto.id}</td>
                                                    <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                                    <td><ul>${descripcion}</ul></td>
                                                    <td>
                                                        <button class="product-delete btn btn-danger">
                                                            Eliminar
                                                        </button>
                                                    </td>
                                                </tr>
                                            `;

                                            template_bar += `
                                                <li>${producto.nombre}</il>
                                            `;
                                        });
                                        // SE HACE VISIBLE LA BARRA DE ESTADO
                                        $('#product-result').show();
                                        // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                                        $('#container').html(template_bar);
                                        // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                                        $('#products').html(template);    
                                    }else{
                                        $('#nombreExiste').text('Nombre valido 🔓');
                                        
                                    }
                                }
                            }
                        })                
                    }
            })            
        
            $('#marca').click(function(){
                var marca = $('#marca option:selected').val();
                console.log(marca);   
                if (marca === "0"){
                    console.log("-Selecciona una marca\n");
                    $('#marcaSpan').text("-Selecciona una marca 🔒\n");
                }else{
                    console.log('Marca correcta');
                    $('#marcaSpan').text('Datos correctos 🔓');
                }
            }) 
        
        
            $('#modelo').keyup(function() {
                var modelo = $('#modelo').val();
                console.log(modelo);
                $('#modelo').blur(function() {
                    if (modelo === null || modelo.length >= 25 || modelo.length === 0) {
                        console.log("-Ingresa una modelo valido\n");
                        $('#modeloSpan').text("-Ingresa una modelo valido 🔒\n");
                    }else{
                        console.log('Modelo correcto');
                        $('#modeloSpan').text('Datos correctos 🔓');
                    }        
                });
            })
        
            $('#precio').keyup(function() {
                var precio = $('#precio').val();
                $('#precio').blur(function() {
                    if (precio <= 99.99) {
                        console.log("-Ingresa un precio mayor a $99.99\n");
                        $('#precioSpan').text('Ingresa un precio mayor a $99.99 🔒\n');
                    }else{
                        console.log('Precio correcto');
                        $('#precioSpan').text('Datos correctos 🔓');
                    }        
                });
            })
        
            $('#detalles').keyup(function() {
                var detalles = $('#detalles').val();
                $('#detalles').blur(function() {
                    if (detalles.length <= 0 || detalles.length >= 250) {
                        console.log("-Ingresa los datos correctamente\n");
                        $('#detallesSpan').text('Ingresa los detalles correctamente🔒\n');
                    }else{
                        console.log('Detalles correctos');
                        $('#detallesSpan').text('Datos correctos 🔓');
                    }        
                });
            })
        
            $('#unidades').keyup(function() {
                var unidades = $('#unidades').val();
                $('#unidades').blur(function() {
                    if (unidades <= 0) {
                        console.log("-Ingresa los datos correctamente\n");
                        $('#unidadesSpan').text('Ingresa un numero de unidades valido 🔒\n');
                    }else{
                        console.log('Datos correctos');
                        $('#unidadesSpan').text('Datos correctos 🔓');
                    }        
                });
            })
        
            $('#imagen').keyup(function() {
                var imagen = $('#imagen').val();
                $('#imagen').blur(function() {
                    if (imagen === null || imagen === "" || imagen.length <= 0) {
                        imagen = "img/porDefecto.png";
                        console.log(imagen);
                    }     
                });
            })      
    }

    $('#dataForm').submit(function(e){
        e.preventDefault();
        let nombreBusqueda = $('#nombreBusqueda').val();
        let marca = $('#marca').val();
        let modelo = $('#modelo').val();
        let precio = $('#precio').val();
        let detalles = $('#detalles').val();
        let unidades = $('#unidades').val();
        let imagen = $('#imagen').val();
        let id = $('#productID').val();

        const postData = {
            nombre: nombreBusqueda,
            marca: marca,
            modelo: modelo,
            precio: precio,
            detalles: detalles,
            unidades: unidades,
            imagen: imagen,
            id: id
        }
    
        console.log(postData);
        console.log(edit);
        
    
        let url = edit == false ? 'backend/product-add.php' : 'backend/product-edit.php';
        console.log(url);
        $.post(url, postData, function(response) {
            console.log(response);
            // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
            let respuesta = JSON.parse(response);
            // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
            let template_bar = '';
            template_bar += `
                        <li style="list-style: none;">status: ${respuesta.status}</li>
                        <li style="list-style: none;">message: ${respuesta.message}</li>
                    `;
            // SE REINICIA EL FORMULARIO
            $('#product-form').trigger('reset');
            // SE HACE VISIBLE LA BARRA DE ESTADO
            $('#product-result').show();
            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
            $('#container').html(template_bar);
            // SE LISTAN TODOS LOS PRODUCTOS
            listarProductos();
            // SE REGRESA LA BANDERA DE EDICIÓN A false
            edit = false;
        });
    })  

    $(document).on('click', '.product-delete', function (e){
        if(confirm('¿Realmente deseas eliminar el producto?')) {
            const element = $(this)[0].parentElement.parentElement;
            const id = $(element).attr('productId');
            $.post('./backend/product-delete.php', {id}, (response) => {
                $('#product-result').hide();
                let template_bar = '';
                template_bar += `
                            <li style="list-style: none;">status: ${respuesta.status}</li>
                            <li style="list-style: none;">message: ${respuesta.message}</li>
                        `;
                $('#container').html(template_bar);
                listarProductos();
            });
        }
    });

    $(document).on('click', '.product-item', function(e){
        const element = $(this)[0].parentElement.parentElement;
        const id = $(element).attr('productId');
        console.log(id);
        $.post('./backend/product-single.php', {id}, function(response) {
            // SE CONVIERTE A OBJETO EL JSON OBTENIDO
            let product = JSON.parse(response);
            // SE INSERTAN LOS DATOS ESPECIALES EN LOS CAMPOS CORRESPONDIENTES
            $('#nombreBusqueda').val(product.nombre);
            $('#marca').val(product.marca);
            $('#modelo').val(product.modelo);
            $('#precio').val(product.precio);
            $('#detalles').val(product.detalles);
            $('#unidades').val(product.unidades);
            $('#imagen').val(product.imagen);
            $('#productId').val(product.id);
            edit = true;
        });
        e.preventDefault();
        listarProductos();
    });  
});