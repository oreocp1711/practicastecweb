<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN” “http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang=“es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title> Practica 4 </title>
</head>

<body>
    <?php
        echo "<h3> El día de hoy es el " . date('d / M / Y H:i:s') . "</h3> <hr/>";
        echo "<h1 >Bienvenido a mi sitio PHP 5 </h1>";

        function ejercicio1(){
            echo "<h4>Ejercicio 1</h4>";
            $num1 = $_GET["numero1"];
            if ($num1 % 5 == 0 and $num1 % 7 == 0){
                echo "El numero $num1 es multiplo de 5 y 7";
            }else{
                echo "El numero $num1 no es multiplo de 5 y 7";
            }
        }

        function ejercicio2(){
            echo "<h4>Ejercicio 2</h4>";
            $contador = 0;
            $estado = false;
            echo "Impar - Par - Impar <br>";
            do{
                $n1 = rand(0, 1000);
                $n2 = rand(0, 1000);
                $n3 = rand(0, 1000);
                echo "-$n1 , -$n2 , -$n3 <br>";
                $data = array(0 => $n1, 1 => $n2, 2 => $n3);
                $matriz = array($contador => $data);
                $contador++;
                if (($n1 % 2 != 0) and ($n2 % 2 == 0) and ($n3 % 2 != 0)){
                    $estado = true;
                }
            }while($estado == false);
            $total = 3*count($matriz);
            $numerosTotal = $total*$contador;
            echo " $numerosTotal numeros obtenidos en $contador iteraciones";
        }

        function ejercicio3(){
            echo "<h4>Ejercicio 3</h4>";
            //$datoFijo = $_GET["dato"];
            $datoFijo = 10;
            $aleatorio = rand(0, 1000);

            while(($aleatorio % $datoFijo) != 0){
                $aleatorio = rand(0, 1000);
                echo "$aleatorio <br>";
            }
            echo "$aleatorio es multiplo $datoFijo";
        }

        function ejercicio31(){
            echo "<h4>Ejercicio 3.1</h4>";
            $datoFijo = $_GET["datoFijo"];
            //$datoFijo = 10;
            $aleatorio = rand(0, 1000);
            do{
                $aleatorio = rand(0, 1000);
                echo "$aleatorio <br>";
            }while(($aleatorio % $datoFijo) != 0);
            echo " -$aleatorio es multiplo $datoFijo";
        }

        function ejercicio4(){
            echo "<h4>Ejercicio 4</h4>";
            $letras = array();
            for($i = 97; $i <= 122; $i++){
                $letras[$i] = chr($i);
            }
            echo '<table border="1">';
            echo "<td>Clave</td>  <td>Valor</td>";
            foreach($letras as $key => $val){
                echo '<tr>';
                echo "<td>$key</td>  <td>$val </td>";
            }
        }

        //ejercicio1();
        //ejercicio2();
        //ejercicio3();
        ejercicio4();

    ?>
</body>

</html>