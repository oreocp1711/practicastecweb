<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN” “http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang=“es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title> Practica 4 </title>
</head>

<body>
    
    <?php

    $matricula = $_POST['matricula'];

    $vehiculos = array(
        "CHL2021" => array(
            "Auto" => array(
                "Marca" => "Mazda",
                "Modelo" => 2021,
                "Tipo" => "Sedan"
            ),
            "Propietario" => array(
                "Nombre" => "Diana Gomez Torres",
                "Ciudad" => "Puebla",
                "Direccion" => "Brrrr"
            )
        ),
        "ARN1999" => array(
            "Auto" => array(
                "Marca" => "Honda",
                "Modelo" => 2019,
                "Tipo" => "Camioneta"
            ),
            "Propietario" => array(
                "Nombre" => "Luis Lopez Ayala",
                "Ciudad" => "Puebla",
                "Direccion" => "Brrrr"
            )
        ),
        "BAR1990" => array(
            "Auto" => array(
                "Marca" => "Ford",
                "Modelo" => 2018,
                "Tipo" => "Camioneta"
            ),
            "Propietario" => array(
                "Nombre" => "Pablo Reyes",
                "Ciudad" => "Puebla",
                "Direccion" => "Miguel Hidalgo 123"
            )
        ),
        "MLA2007" => array(
            "Auto" => array(
                "Marca" => "Hyundai",
                "Modelo" => 2015,
                "Tipo" => "Sedan"
            ),
            "Propietario" => array(
                "Nombre" => "Mario Bautista",
                "Ciudad" => "Puebla",
                "Direccion" => "Resurreccion 65"
            )
        ),
        "RMD2017" => array(
            "Auto" => array(
                "Marca" => "Audi",
                "Modelo" => 2019,
                "Tipo" => "Sedan"
            ),
            "Propietario" => array(
                "Nombre" => "Jesus Alejandro",
                "Ciudad" => "Puebla",
                "Direccion" => "Tres de Mayo 14"
            )
        ),
        "AMR2001" => array(
            "Auto" => array(
                "Marca" => "Jeep",
                "Modelo" => 2021,
                "Tipo" => "Camioneta"
            ),
            "Propietario" => array(
                "Nombre" => "Luis Fernando Lancho Tlatelpa",
                "Ciudad" => "Puebla",
                "Direccion" => "Santa Anita"
            )
        ),
        "PAC2016" => array(
            "Auto" => array(
                "Marca" => "BMW",
                "Modelo" => 2020,
                "Tipo" => "Sedan"
            ),
            "Propietario" => array(
                "Nombre" => "Eugenia Vazquez Mora",
                "Ciudad" => "Puebla",
                "Direccion" => "San Esteban"
            )
        ),
        "MST9090" => array(
            "Auto" => array(
                "Marca" => "Honda",
                "Modelo" => 2020,
                "Tipo" => "Camioneta"
            ),
            "Propietario" => array(
                "Nombre" => "Victor Rodriguez",
                "Ciudad" => "Puebla",
                "Direccion" => "Xonacatepec 189"
            )
        ),
        "NOL2001" => array(
            "Auto" => array(
                "Marca" => "Mazda",
                "Modelo" => 2021,
                "Tipo" => "Sedan"
            ),
            "Propietario" => array(
                "Nombre" => "Iman Vazquez Zenteno",
                "Ciudad" => "Puebla",
                "Direccion" => "Amozoc"
            )
        ),
        "GAB1406" => array(
            "Auto" => array(
                "Marca" => "Nissan",
                "Modelo" => 2021,
                "Tipo" => "Sedan"
            ),
            "Propietario" => array(
                "Nombre" => "Yuliana Juarez Vazquez",
                "Ciudad" => "Puebla",
                "Direccion" => "San Martin 87"
            )
        ),
        "FRI2005" => array(
            "Auto" => array(
                "Marca" => "Kia",
                "Modelo" => 2021,
                "Tipo" => "Camioneta"
            ),
            "Propietario" => array(
                "Nombre" => "Mario Cardona",
                "Ciudad" => "Puebla",
                "Direccion" => "97 Oriente"
            )
        ),
        "SON1232" => array(
            "Auto" => array(
                "Marca" => "Mazda",
                "Modelo" => 2021,
                "Tipo" => "Sedan"
            ),
            "Propietario" => array(
                "Nombre" => "Adrian Palancares Gavito",
                "Ciudad" => "Puebla",
                "Direccion" => "La Resurrecion 65"
            )
        ),
        "BRR2021" => array(
            "Auto" => array(
                "Marca" => "VW",
                "Modelo" => 2018,
                "Tipo" => "Camioneta"
            ),
            "Propietario" => array(
                "Nombre" => "Efrain Juarez Garcia",
                "Ciudad" => "Puebla",
                "Direccion" => "Bosques 87"
            )
        ),
        "BOT2018" => array(
            "Auto" => array(
                "Marca" => "Nissan",
                "Modelo" => 2021,
                "Tipo" => "Sedan"
            ),
            "Propietario" => array(
                "Nombre" => "Ximena Peralta Luna",
                "Ciudad" => "Puebla",
                "Direccion" => "San Manuel 87"
            )
        ),
        "FER2109" => array(
            "Auto" => array(
                "Marca" => "VW",
                "Modelo" => 2019,
                "Tipo" => "Camioneta"
            ),
            "Propietario" => array(
                "Nombre" => "Ricardo Cariño",
                "Ciudad" => "Puebla",
                "Direccion" => "17 Norte"
            )
        ),

    );

    function getInfo($vehiculos, $matricula){
        if (array_key_exists($matricula, $vehiculos)){
            echo "Informacion de la matricula: $matricula: <br>";
            echo('<pre>');
            print_r($vehiculos[$matricula]);
            echo('</pre>');
        }else{
            echo "Informacion de la matricula es incorrecta";
        }
    }

    getInfo($vehiculos, $matricula);
  

    ?>
</body>

</html>