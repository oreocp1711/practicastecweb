<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title> Practica 6 </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
</head>

<?php

    $user = "root";
    $pass = "nole123456";
    $server = "127.0.0.1";
    $db = "marketzone";
    @$conectar = new mysqli($server, $user, $pass, $db);

    /** Comprobar la conexión */
    if ($conectar->connect_errno) {
        die('Falló la conexión: ' . $link->connect_error . '<br/>');
        //exit();
    }

    // Hacemos la consulta a la base de datos
    $consultar = "SELECT * FROM productos WHERE eliminado != 1";
    // Guardamos la consulta realizada
    $query = mysqli_query($conectar, $consultar);
    $array = mysqli_fetch_array($query);
    
    
?>

<body>
<div class="container py-4">
<h1>Practica 6</h1>
        <table class="table mb-3">
            <thead class="thead-dark">
                <tr>
					<th scope="col">ID</th>
					<th scope="col">Nombre</th>
					<th scope="col">Marca</th>
					<th scope="col">Modelo</th>
					<th scope="col">Precio</th>
					<th scope="col">Unidades</th>
					<th scope="col">Detalles</th>
					<th scope="col">Imagen</th>
			    </tr>
            </thead>
            <tbody>
            <?php foreach ($query as $row){ ?>
                <tr>
					<th scope="row"><?= $row['id'] ?></th>
					<td><?= $row['nombre'] ?></td>
					<td><?= $row['marca'] ?></td>
					<td><?= $row['modelo'] ?></td>
					<td><?= $row['precio'] ?></td>
					<td><?= $row['unidades'] ?></td>
					<td><?= utf8_encode($row['detalles']) ?></td>
					<td><img src="<?= $row['imagen'] ?>" style="width:70%"/></td>
				</tr>
            </tbody>
        <?php
            }
        ?>
        </table>
        </div>

</body>

</html>