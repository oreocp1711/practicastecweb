<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <title>Formulario Producto</title>
</head>
<body>
    <div class="container py-3">
        <form action="set_producto_v2.php" method="post">
            <h2>Ingrese los datos requeridos</h2>
            <div class="mb-3">
                <label for="nombre" class="form-label">Nombre del producto: </label>
                <input type="text" required="required" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
            </div>
            <div class="mb-3">
                <label for="marca" class="form-label">Marca: </label>
                <input type="text" required="required" class="form-control" id="marca" name="marca" placeholder="Marca">                
            </div>
            <div class="mb-3">
                <label for="modelo" class="form-label">Modelo</label>
                <input type="text" required="required" class="form-control" id="modelo" name="modelo" placeholder="Modelo">
            </div>
            <div class="mb-3">
                <label for="precio" class="form-label">Precio</label>
                <input type="text" pattern="^(0(?!\.00)|[1-9]\d{0,6})\.\d{2}$" required="required" class="form-control" id="precio" name="precio" placeholder="$0.00">
            </div>
            <div class="mb-3">
                <label for="detalles" class="form-label">Detalles</label>
                <input type="text" required="required" class="form-control" id="detalles" name="detalles" maxlength="250" placeholder="Detalles">
            </div>
            <div class="mb-3">
                <label for="unidades" class="form-label">Unidades</label>
                <input type="number" required="required" class="form-control" id="unidades" name="unidades" placeholder="0">
            </div>
            <div class="mb-3">
                <label for="imagen" class="form-label">Imagen</label>
                <input type="text" required="required" class="form-control" id="imagen" name="imagen" placeholder="Imagen">
            </div>
            <div class="col-12 mb-3">
                <button type="submit" class="btn btn-dark">Enviar</button>
            </div>
        </form>        
    </div>
</body>
</html>