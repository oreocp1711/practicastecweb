<?php
namespace API;
abstract class DataBase {
    protected $conexion;
    protected $response;

    public function __construct($database) {
        $this->response = array();
        $this->conexion = @mysqli_connect(
            'localhost',
            'root',
            'nole123456',
            $database
        );
        if(!$this->conexion) {
            die('¡Base de datos NO conextada!');
        }
    }
    public function getResponse() {
        // SE HACE LA CONVERSIÓN DE ARRAY A JSON
        return json_encode($this->response, JSON_PRETTY_PRINT);
    }
}
?>