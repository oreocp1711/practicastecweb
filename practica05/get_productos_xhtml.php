<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN” “http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang=“es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title> Practica 5 </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<?php
    //$tope = $_GET["tope"];
    if (isset($_GET['tope'])) {
        $tope = $_GET['tope'];
    } else {
        die('Parámetro "tope" no detectado...');
    }

    if (!empty($tope)){
        $user = "root";
        $pass = "nole123456";
        $server = "127.0.0.1";
        $db = "marketzone";
        @$conectar = new mysqli($server, $user, $pass, $db);

        /** Comprobar la conexión */
        if ($conectar->connect_errno) {
            die('Falló la conexión: ' . $link->connect_error . '<br/>');
            //exit();
        }

        // Hacemos la consulta a la base de datos
        $consultar = "SELECT * FROM productos WHERE unidades <= $tope";
        // Guardamos la consulta realizada
        $query = mysqli_query($conectar, $consultar);
        $array = mysqli_fetch_array($query);
    }
    
    
?>

<body>
<h1>Practica 5</h1>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Marca</th>
                    <th scope="col">Modelo</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Unidades</th>
                    <th scope="col">Detalles</th>
                    <th scope="col">Imagen</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($query as $row){ ?>
                <tr>
                    <th scope="data"><?= $row['id'] ?></th>
                    <td><?= $row['nombre'] ?></td>
                    <td><?= $row['marca'] ?></td>
                    <td><?= $row['modelo'] ?></td>
                    <td><?= $row['precio'] ?></td>
                    <td><?= $row['unidades'] ?></td>
                    <td><?= utf8_encode($row['detalles']) ?></td>
                    <td><img style="width:30%" src=<?= $row['imagen'] ?>></td>
                </tr>
            </tbody>
        <?php
            }
        ?>
        </table>


</body>

</html>