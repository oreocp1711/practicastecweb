<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN” “http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang=“es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title> Practica 5 </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script>
              function show() {
                // se obtiene el id de la fila donde está el botón presinado
                var rowId = event.target.parentNode.parentNode.id;
                console.log(typeof(rowId));
                // se obtienen los datos de la fila en forma de arreglo
                var data = document.getElementById(rowId).querySelectorAll(".row-data");
                console.log(data.length);
                /**
                querySelectorAll() devuelve una lista de elementos (NodeList) que 
                coinciden con el grupo de selectores CSS indicados.
                (ver: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)

                En este caso se obtienen todos los datos de la fila con el id encontrado
                y que pertenecen a la clase "row-data".
                */

                var id = parseInt(data[0].innerHTML);
                var nombre = data[1].innerHTML;
                var marca = data[2].innerHTML;
                var modelo = data[3].innerHTML;
                var precio = data[4].innerHTML;
                var unidades = parseInt(data[5].innerHTML);
                var detalles = data[6].innerHTML;
                var imagenNueva = data[7].firstChild.getAttribute('src');
                

                /*
                var imagen = data[6].innerHTML;
                var indice1 = imagen.indexOf('/')-3;
                var indice2 = imagen.indexOf('.')+4;
                var imagenNueva = imagen.slice(indice1, indice2);
                */
                console.log(unidades);
                console.log(detalles);
                console.log("Id es " + id);
                //console.log(imagen);

                console.log("La marca es " + marca);
                console.log(typeof(marca));
                console.log("Info ...............");

                alert(
                    "ID: " + id + 
                    "\nName: " + nombre + 
                    "\nMarca: " + marca +
                    "\nModelo: " + modelo +
                    "\nPrecio: " + precio +
                    "\nUnidades: " + unidades +
                    "\nDetalles: " + detalles 
                );

                send2form(id, nombre, marca, modelo, precio, unidades, detalles, imagenNueva );

            } 
        </script>
</head>

<?php
    //$tope = $_GET["tope"];
    if (isset($_GET['tope'])) {
        $tope = $_GET['tope'];
    } else {
        die('Parámetro "tope" no detectado...');
    }

    if (!empty($tope)){
        $user = "root";
        $pass = "nole123456";
        $server = "127.0.0.1";
        $db = "marketzone";
        @$conectar = new mysqli($server, $user, $pass, $db);

        /** Comprobar la conexión */
        if ($conectar->connect_errno) {
            die('Falló la conexión: ' . $link->connect_error . '<br/>');
            //exit();
        }

        // Hacemos la consulta a la base de datos
        $consultar = "SELECT * FROM productos WHERE unidades <= $tope";
        // Guardamos la consulta realizada
        $query = mysqli_query($conectar, $consultar);
        $array = mysqli_fetch_array($query);
    }
    
    
?>

<body>
<div class="container py-4">
    <h1>Practica 5</h1>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Unidades</th>
                        <th scope="col">Detalles</th>
                        <th scope="col">Imagen</th>
                        <th scope="col">Modificar</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($query as $row){ ?>
                    <tr id="<?= $row['id'] ?>">
					<th scope="row"><?= $row['id'] ?></th>
					<td class="row-data" ><?= $row['nombre'] ?></td>
					<td class="row-data" ><?= $row['marca'] ?></td>
					<td class="row-data" ><?= $row['modelo'] ?></td>
					<td class="row-data" ><?= $row['precio'] ?></td>
					<td class="row-data" ><?= $row['unidades'] ?></td>
					<td class="row-data" ><?= utf8_encode($row['detalles']) ?></td>
					<td class="row-data" ><img src="<?= $row['imagen'] ?>" style="width:70%"/></td>
                    <td>
                        <input type="button" class="btn btn-dark" value="Editar" onclick="show();" />
                    </td>
               </tr>
                </tbody>
            <?php
                }
            ?>
            </table>
</div>

<script>
    function send2form(id, nombre, marca, modelo, precio, unidades, detalles, imagenNueva) {     //form) { 
        var urlForm = "formulario_productos_v2Mod.php";
        var propNombre = "nombre="+nombre;
        var propMarca = "marca="+marca;
        var propModelo = "modelo="+modelo;
        var propPrecio = "precio="+precio;
        var propDetalles = "detalles="+detalles;
        var propUnidades = "unidades="+unidades;
        var propImagen = "imagen="+imagenNueva;

        window.open(urlForm+"?"+propNombre+"&"+propMarca+"&"+propModelo+"&"+propPrecio+"&"+propUnidades+"&"+propDetalles+"&"+propImagen);
    }
</script>

</body>

</html>