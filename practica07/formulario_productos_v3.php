<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <title>Formulario Producto</title>
</head>
<body>
    <div class="container py-3">
        <form id="datos" action="actualizar.php" method="post">
            <h2>Ingrese los datos requeridos</h2>
            
            <div class="mb-3">
                    Tu id es: <?php isset($_POST["id"]) ? print $_POST["id"] : ""; ?>
                    <input type="text" name="id" value="<?= !empty($_POST['id'])?$_POST['id']:$_GET['id'] ?>" >
            </div>
            <div class="mb-3">
                <label for="nombre" class="form-label">Nombre del producto: </label>
                <input type="text" required="required" class="form-control" id="nombre" name="nombre" maxlength="100" placeholder="Nombre"
                    value="<?= !empty($_POST['nombre'])?$_POST['nombre']:$_GET['nombre'] ?>"
                >
            </div>
            <div class="mb-3">
                <label for="marca" class="form-label">Marca: </label>
                <select  required="required" class="form-select" id="marca" name="marca">
                    <option disabled selected hidden="disabled" value="<?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?>"> 
                        <?php 
                        if(!empty($_POST['marca'])){
                            print_r($_POST['marca']);
                        }else{
                            print_r($_GET['marca']);
                        }
                        ?> 
                    </option>
                    <option value="Nike">Nike</option>
                    <option value="Adidas">Adidas</option>
                    <option value="Vans">Vans</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="modelo" class="form-label">Modelo: </label>
                <input type="text" required="required" class="form-control" id="modelo" name="modelo" maxlength="25" placeholder="Modelo"
                    value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>"
                >
            </div>
            <div class="mb-3">
                <label for="precio" class="form-label">Precio: </label>
                <input type="text" pattern="^(0(?!\.00)|[1-9]\d{0,6})\.\d{2}$" required="required" class="form-control" id="precio" name="precio" placeholder="$0.00"
                    value="<?= !empty($_POST['precio'])?$_POST['precio']:$_GET['precio'] ?>"
                >
            </div>
            <div class="mb-3">
                <label for="detalles" class="form-label">Detalles: </label>
                <input type="text" class="form-control" id="detalles" name="detalles" maxlength="250" placeholder="Detalles"
                    value="<?= !empty($_POST['detalles'])?$_POST['detalles']:$_GET['detalles'] ?>"
                >
            </div>            
            <div class="mb-3">
                <label for="unidades" class="form-label">Unidades: </label>
                <input type="number" required="required" class="form-control" id="unidades" name="unidades" placeholder="0"
                    value="<?= !empty($_POST['unidades'])?$_POST['unidades']:$_GET['unidades'] ?>"
                >
            </div>
            <div class="mb-3">
                <label for="imagen" class="form-label">Imagen: </label>
                <input type="text" class="form-control" id="imagen" name="imagen" placeholder="Imagen"
                    value="<?= !empty($_POST['imagen'])?$_POST['imagen']:$_GET['imagen'] ?>"
                >
            </div>
            <div class="col-12 mb-3">
                <button type="submit" id="btnEnviar" class="btn btn-dark">Enviar</button>
            </div>
        </form>        
    </div>

</body>
<script src="js/validacion.js"></script>
</html>