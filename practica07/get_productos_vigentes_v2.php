<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title> Practica 7 </title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script>
              function show() {
                // se obtiene el id de la fila donde está el botón presinado
                var rowId = event.target.parentNode.parentNode.id;
                console.log(typeof(rowId));
                // se obtienen los datos de la fila en forma de arreglo
                var data = document.getElementById(rowId).querySelectorAll(".row-data");
                console.log(data.length);
                /**
                querySelectorAll() devuelve una lista de elementos (NodeList) que 
                coinciden con el grupo de selectores CSS indicados.
                (ver: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)

                En este caso se obtienen todos los datos de la fila con el id encontrado
                y que pertenecen a la clase "row-data".
                */

                var id = parseInt(data[0].innerHTML);
                var nombre = data[1].innerHTML;
                var marca = data[2].innerHTML;
                var modelo = data[3].innerHTML;
                var precio = data[4].innerHTML;
                var unidades = parseInt(data[5].innerHTML);
                var detalles = data[6].innerHTML;
                var imagenNueva = data[7].firstChild.getAttribute('src');
                

                /*
                var imagen = data[6].innerHTML;
                var indice1 = imagen.indexOf('/')-3;
                var indice2 = imagen.indexOf('.')+4;
                var imagenNueva = imagen.slice(indice1, indice2);
                */
                console.log(unidades);
                console.log(detalles);
                console.log("Id es " + id);
                //console.log(imagen);

                console.log("La marca es " + marca);
                console.log(typeof(marca));
                console.log("Info ...............");

                alert(
                    "ID: " + id + 
                    "\nName: " + nombre + 
                    "\nMarca: " + marca +
                    "\nModelo: " + modelo +
                    "\nPrecio: " + precio +
                    "\nUnidades: " + unidades +
                    "\nDetalles: " + detalles 
                );

                send2form(id, nombre, marca, modelo, precio, unidades, detalles, imagenNueva );

            } 
    </script>
</head>

<?php

    $user = "root";
    $pass = "nole123456";
    $server = "127.0.0.1";
    $db = "marketzone";
    @$conectar = new mysqli($server, $user, $pass, $db);

    /** Comprobar la conexión */
    if ($conectar->connect_errno) {
        die('Falló la conexión: ' . $link->connect_error . '<br/>');
        //exit();
    }

    // Hacemos la consulta a la base de datos
    $consultar = "SELECT * FROM productos WHERE eliminado != 1";
    // Guardamos la consulta realizada
    $query = mysqli_query($conectar, $consultar);
    $array = mysqli_fetch_array($query);
    
    
?>

<body>
<div class="container py-4">
<h1>Practica 7</h1>
        <table class="table mb-3">
            <thead class="thead-dark">
                <tr>
					<th scope="col">ID</th>
					<th scope="col">Nombre</th>
					<th scope="col">Marca</th>
					<th scope="col">Modelo</th>
					<th scope="col">Precio</th>
					<th scope="col">Unidades</th>
					<th scope="col">Detalles</th>
					<th scope="col">Imagen</th>
                    <th scope="col">Modificar</th>
			    </tr>
            </thead>
            <tbody>
            <?php foreach ($query as $row){ ?>
                <tr  id="<?= $row['id'] ?>">
					<th class="row-data" scope="row"><?= $row['id'] ?></th>
					<td class="row-data" ><?= $row['nombre'] ?></td>
					<td class="row-data" ><?= $row['marca'] ?></td>
					<td class="row-data" ><?= $row['modelo'] ?></td>
					<td class="row-data" ><?= $row['precio'] ?></td>
					<td class="row-data" ><?= $row['unidades'] ?></td>
					<td class="row-data" ><?= utf8_encode($row['detalles']) ?></td>
					<td class="row-data" ><img src="<?= $row['imagen'] ?>" style="width:70%"/></td>
                    <td>
                        <input type="button" class="btn btn-dark"  value="Editar" onclick="show();" />
                    </td>
               </tr>
            </tbody>
            <?php
            }
        ?>
        </table>
        </div>
        <script>

            function send2form(id, nombre, marca, modelo, precio, unidades, detalles, imagenNueva ) {     //form) { 
                var form = document.createElement("form");
                console.log(nombre);
                console.log("Procesando... " + marca)

                var idIn = document.createElement("input");
                idIn.type = 'number';
                idIn.name = 'id';
                idIn.value = id;
                form.appendChild(idIn);

                var nombreIn = document.createElement("input");
                nombreIn.type = 'text';
                nombreIn.name = 'nombre';
                nombreIn.value = nombre;
                form.appendChild(nombreIn);

                var marcaIn = document.createElement("input");
                marcaIn.type = 'text';
                marcaIn.name = 'marca';
                marcaIn.value = marca;
                form.appendChild(marcaIn);

                var modeloIn = document.createElement("input");
                modeloIn.type = 'text';
                modeloIn.name = 'modelo';
                modeloIn.value = modelo;
                form.appendChild(modeloIn);

                var precioIn = document.createElement("input");
                precioIn.type = 'text';
                precioIn.name = 'precio';
                precioIn.value = precio;
                form.appendChild(precioIn);

                var unidadesIn = document.createElement("input");
                unidadesIn.type = 'number';
                unidadesIn.name = 'unidades';
                unidadesIn.value = unidades;
                form.appendChild(unidadesIn);

                var detalleIn = document.createElement("input");
                detalleIn.type = 'text';
                detalleIn.name = 'detalles';
                detalleIn.value = detalles;
                form.appendChild(detalleIn);

                var imagenIn = document.createElement("input");
                imagenIn.type = 'text';
                imagenIn.name = 'imagen';
                imagenIn.value = imagenNueva;
                form.appendChild(imagenIn);



                //console.log(form);

                form.method = 'POST';
                form.action = 'formulario_productos_v3.php';  

                document.body.appendChild(form);
                form.submit();
                console.log(nombreIn);
            }

        </script>

</body>

</html>