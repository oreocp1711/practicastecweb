var nombre = document.getElementById("nombre");
var marca = document.getElementById("marca");
var modelo = document.getElementById("modelo");
var price = document.getElementById("precio");
var detalles = document.getElementById("detalles");
var unidades = document.getElementById("unidades");
var imagen = document.getElementById("imagen");

const btnEnviar = document.getElementById("btnEnviar");

/*

function validar(){
    console.log("Enivando formulario");
    var errrores = [];

    if(nombre.value === null || nombre.value.length >= 100){
        errrores.push("Ingresa un nombre valido");
    }
    if(modelo.value === null || modelo.value.length >= 25){
        errrores.push("Ingresa una descripcion valida");
    }
    if(nPrecio <= 99.99){
        errrores.push("Ingresa un precio mayor");
    }
    if(detalles.value === null || detalles.value.length >= 100){
        errrores.push("Ingresa un nombre valido");
    }
    if(unidades.value <= 0 ){
        errrores.push("Ingresa una cantidad valida");
    }
    if(imagen.value == null){
        imagen = "porDefecto.png";
    }

    alert(errrores.join(", "));

    return false;
}
*/

var form = document.getElementById("datos");
form.addEventListener("submit", function(event){
    event.preventDefault();

    var errrores = [];

    if(nombre.value === null || nombre.value.length >= 100){
        errrores.push("-Ingresa un nombre valido\n");
    }
    if(marca.value === "0"){
        errrores.push("-Selecciona una marca\n");
    }
    if(modelo.value === null || modelo.value.length >= 25){
        errrores.push("-Ingresa una descripcion valida\n");
    }
    if(precio.value <= 99.99){
        console.log(precio.value);
        errrores.push("-Ingresa un precio mayor a $99.99\n");
    }
    if(detalles.value.length <= 0 || detalles.value.length >= 250){
        errrores.push("-Ingresa una descripcion valida\n");
    }
    if(unidades.value <= 0 ){
        errrores.push("-Ingresa una cantidad valida\n");
    }
    if(imagen.value === null || imagen.value === "" || imagen.value.length <= 0){
        //errrores.push("-Ingresa una direccion valida\n");
        imagen.value = "porDefecto.png";
        console.log(imagen.value);
    }

    console.log("Enviando formulario");
    console.log(nombre.value);
    console.log(marca.value);
    console.log(modelo.value);
    console.log(precio.value);
    console.log(detalles.value);
    console.log(unidades.value);
    console.log(imagen.value);

    
    
    if(errrores.length > 0){
        alert(errrores.join(" "));
    }else{
        this.submit();
    }
    
    
});

