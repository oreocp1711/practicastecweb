<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN” “http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang=“es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title> Practica 3 </title>
</head>

<body>
    <?php
        echo "<h3> El día de hoy es el " . date('d / M / Y H:i:s') . "</h3> <hr/>";
        echo "<h1 >Bienvenido a mi sitio PHP 5 </h1>";

        function ejercicio1(){
            echo "<h4>Ejercicio 1</h4>";
            echo "\$_myvar; -- El iniciar con underscore es valido <br>";
            echo "\$_7var;  -- Es una valida el iniciar con underscore, en caso de no tenerla si marcaria un error al inicair con un numero <br>";
            echo "myvar;   -- Este no es posible y marcara un error al faltar el signo '$' <br>";
            echo "\$myvar;  -- Caso contrario al anterior en este caso la variable es valida <br>";
            echo "\$var7;   -- Es un caso valido <br>";
            echo "\$_element1; -- Es un caso valido <br>";
            echo "\$house*5;   -- Es una asignacion no valida por el signo de '*', marcara error <br>";
        }

        function ejercicio2(){
            echo "<h4>Ejercicio 2</h4>";
            $a = "Manejador SQL";
            $b = "MySQL";
            $c = &$a;
            //  Mostramos el contenido de cada variable
            echo " \$a = $a  \$b = $b  \$c = $c <br>";
            //  Agregamos las siguientes asignaciones
            $a = "PHP server";
            $b = &$a;
            //  Volvemos a mostrar el contenido de cada variable
            echo " \$a = $a  \$b = $b  \$c = $c <br>";
            echo "<p>
            El valor de \$a fue reasignado por 'PHP server', el valor de \$b fue asignado por refencia hacia a la variable \$a,
            el caso de \$c es igual a \$b solo que \$c sigue manteniendo el valor por referencia que desde un principio fue asignado con la variable \$a. <br>
            </p>";
        }

        function ejercicio3(){
            echo "<h4>Ejercicio 3</h4>";
            $a = "PHP5 ";
            echo "$a <br>";
            $z[] = &$a;
            echo print_r($z);
            $b = "5a version de PHP ";
            echo "<br> $b <br>";
            // Me marcaba un erro al hacer la asignacion de c
            //$c = $b * 10;
            // Y supuse que el valor de b debe repetirse 10 veces asi que use una funcion str_repeat()
            $c = str_repeat($b, 10);
            echo "<br> $c <br>";
            $a .= $b;
            echo "<br> $a <br>";
            $b .= $c;
            echo "<br> $b <br>";
            $z[0] = "MySQL";
            print_r($z);
        }

        function ejercicio4(&$z){
            echo "<h4>Ejercicio 4</h4>";
            $z = print_r($z);
            echo $GLOBALS['a'] . $GLOBALS['b'] . $GLOBALS['c'] . $GLOBALS['z']; 
        }

        function ejercicio5(){
            echo "<h4>Ejercicio 5</h4>";
            $a = "7 personas";
            $b = (integer) $a;
            $a = "9E3";
            $c = (double) $a;
            echo "-$a - $b -$c";
        }        

        function ejercicio6(){
            echo "<h4>Ejercicio 6</h4>";
            $a = "0";
            $b = "TRUE";
            $c = FALSE;
            $d = ($a OR $b);
            $e = ($a AND $c);
            $f = ($a XOR $b);
            echo "Valor de la variable a = ";
            var_dump($a);
            echo "<br>";
            echo "Valor de la variable b = ";
            var_dump($b);
            echo "<br>";
            echo "Valor de la variable c = ";
            var_dump($c);
            echo "<br>";
            echo "Valor de la variable d = ";
            var_dump($d);
            echo "<br>";
            echo "Valor de la variable e = ";
            var_dump($e);
            echo "<br>";
            echo "Valor de la variable f = ";
            var_dump($f);
            echo "<br>";
            echo "Recordemos que '1' = Verdadero(True) '0' = Falso(False) <br>";
            echo "Valor de la variable c = ";
            echo (int)$c;
            echo "<br>Valor de la variable e = ";
            echo (int)$e;
        }

        function ejercicio7(){
            echo "<h4>Ejercicio 7</h4>";
            echo "Version de Apache y PHP  <br> ";
            echo $_SERVER['SERVER_SIGNATURE'];
            echo "<br> Nombre del sistema operativo <br> ";
            echo $_SERVER['SERVER_NAME'];
            echo" <br>";
            echo "<br> Idioma del navegador  <br> ";
            echo $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        }

        //ejercicio1();
        //ejercicio2();
        //ejercicio3();

        $a = "PHP5 ";
        $z[] = &$a;
        $b = "5a version de PHP ";
        $c = str_repeat($b, 10);
        $a .= $b;
        $b .= $c;
        $z[0] = "MySQL";
        //ejercicio4($z);

        //ejercicio5();
        //ejercicio6();
        ejercicio7();


    ?>
</body>

</html>